﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MyDotNetWebProject.Controllers
{
    public class StudentsController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Bennett()
        {
            ViewData["Name"] = "Brian T. Bennett, PhD";
            return View();
        }

        public IActionResult Daniel()
        {
            ViewData["Name"] = "John Daniel";
            return View();
        }

        public IActionResult Kerns ()
        {
            ViewData["Name"] = "Nikole Kerns";
            return View();
        }

        public IActionResult Lane()
        {
            ViewData["Name"] = "Benjamin S. Lane, BaS";
            return View();
        }
        public IActionResult Manna()
        {
            ViewData["Name"] = "Madeline Manna";
            return View();
        }

        public IActionResult Onek()
        {
            ViewData["Name"] = "Tristan Onek";
            return View();
        }

        public IActionResult Vines()
        {
            ViewData["Name"] = "Jacob Vines";
            return View();
        }

        public IActionResult Snoddy()
        {
            ViewData["Name"] = "Mary Snoddy";
            return View();
        }
        public IActionResult Tucker()
        {
            ViewData["Name"] = "Micaela Tucker";
            return View();
        }
		public IActionResult Feathers()
		{
			ViewData["Name"] = "Clayton Feathers";
			return View();
		}
		public IActionResult Pitman()
		{
			ViewData["Name"] = "Anthony Pitman";
			return View();
		}

        public IActionResult Varner()
        {
            ViewData["Name"] = "Jerick Varner";
            return View();
        }
    }
}
